import React, { Component } from 'react';
import logoSimplon from './ecole_numerique.png';
import logoSimplon2 from './logo.png';
import Cartes from './Cartes.js'
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">

          <div className="hautPage">
            <img src={logoSimplon} className="logoSimplon" alt="blabla"/>
              <div className="titres">
                <h1>Simplon Occitannie </h1>
                <h2>Promotion 2018-2019 </h2>
              </div>
            <img src={logoSimplon2} className="App-logo" alt="blabla" />
          </div> 

           <div className="Cartes">
            <Cartes/>
          </div> 
        </header>
      </div>
    );
  }
}

export default App;
